# Recipe App API Proxy

NGINX proxy app for a recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app (default: `app`)
* `APP_PORT` - Port of the app (default:`9000`)

